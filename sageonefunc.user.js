// ==UserScript==
// @name         Sage Funcionalidades
// @version      1.0
// @downloadURL  https://bitbucket.org/teuzer/sageone/raw/master/sageonefunc.user.js
// @updateURL    https://bitbucket.org/teuzer/sageone/raw/master/sageonefunc.user.js
// @description  SAGEONE FUNCIONALIDADES
// @author       Teuzer
// @match        https://app.br.sageone.com/*
// @match        https://*.ao3tech.com/*
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// ==/UserScript==
$(function(){
    if(window.location == window.parent.location){
    var dwi = $(document).outerWidth();
    var my = 200;
	var mymenuwi = ((my*100)/dwi);
    var docuwi = 100-mymenuwi;

    $("html").first().css({'position':'relative','width':docuwi+'%'});
    $("#ui-globalnav, #ui-head").css({'width':docuwi+'%'});
	$(".serviceuser").first().append("<div id='minhabar' style='position:fixed;z-index:99999;width:"+mymenuwi+"%;height:100%;top:0;right:0;text-align:center;background:white;color:white;border-left:1px solid #ccc;padding:5px;font-size:16px;font-weight:bolder;'></div>");
	//$("#main-menu>ul.UIAnimatedMenu>li.item_2>a.UILink").css({'background':'red'});
	$("#minhabar").append("<div id='newconsumidor' style='position:relative;width:190px;height:40px;float:left;background:#00cc66;border-radius:10px;padding:10px;border:1px solid black;cursor:pointer;margin:10px auto 10px;'><i class='fa fa-sticky-note-o' aria-hidden='true' style='margin-right:5px;'></i>Emitir Consumidor</div>");
	$("#minhabar").append("<div id='newsaida' style='position:relative;width:190px;height:40px;float:left;background:#3366ff;border-radius:10px;padding:10px;border:1px solid black;cursor:pointer;margin:10px auto 10px;'><i class='fa fa-file-text-o' aria-hidden='true' style='margin-right:5px;'></i>Emitir Saída</div>");
    $("#minhabar").append("<div  style='position:relative;width:100%;height:30px;color:#000;float:left;padding-top:5px;border-top:1px solid black;margin:10px auto 10px;'>Historico Notas Emitidas</div>");

    $("#minhabar").append("<div id='consumidor' style='position:relative;width:190px;height:40px;float:left;background:#00cc66;border-radius:10px;padding:10px;border:1px solid black;cursor:pointer;margin:10px auto 10px;'>Consumidor</div>");
    $("#minhabar").append("<div id='saida' style='position:relative;width:190px;height:40px;float:left;background:#3366ff;border-radius:10px;padding:10px;border:1px solid black;cursor:pointer;margin:10px auto 10px;'>Saida</div>");


   // $("#user-menu").css({'right':'250px'});
    // Your code here...
    $("#newconsumidor").on('click',function(){
		window.location.href = "https://nfe.ao3tech.com/notafiscal/nfces/new";
	});
	$("#newsaida").on('click',function(){
		window.location.href = "https://nfe.ao3tech.com/notafiscal/nfes/new";
	});
    $("#consumidor").on('click',function(){
		window.location.href = "https://nfe.ao3tech.com/notafiscal/nfces";
	});
	$("#saida").on('click',function(){
		window.location.href = "https://nfe.ao3tech.com/notafiscal/nfes";
	});
    }
});

$( window ).resize(function() {
    if(window.location != window.parent.location){
	var dwi = $(document).outerWidth();
    var my = 200;
	var mymenuwi = ((my*100)/dwi);
    var docuwi = 100-mymenuwi;

    $("html").css({'position':'relative','width':docuwi+'%'});
    $("#ui-globalnav, #ui-head").css({'width':docuwi+'%'});
    $("#minhabar").css({'width':mymenuwi+'%'});
    }
});